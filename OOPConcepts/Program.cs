﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPConcepts
{
    interface IRectangle
    {
         int calculateRectangleArea(int l, int b );
    }
    interface ICircle
    {
        double calculateCircleArea(double r);
    }

    class Rectangle : IRectangle
    {
        private int length = 10;
        private int breadth = 20;
        public int calculateRectangleArea(int l, int b)
        {
            return l * b;
        }
        public void calculateRectangleArea()
        {
            Console.WriteLine("Area of Rectangle is:" + length * breadth);
        }

    }
    class Circle : ICircle
    {
        private double radius = 12.43;
        public double calculateCircleArea(double r)
        {
            return Math.PI * radius * radius;
        }
    }
    class Program:Rectangle
    {
        static void Main(string[] args)
        {
            Program p1 = new Program();
            Circle c1 = new Circle();
            Console.WriteLine("Area of circle is:" +c1.calculateCircleArea(12.43));
            Rectangle r1 = new Rectangle();
            Console.WriteLine("Area of Rectangle is:" + r1.calculateRectangleArea(20,12));
            p1.calculateRectangleArea();
            Console.ReadLine();
            
        }
    }

}


