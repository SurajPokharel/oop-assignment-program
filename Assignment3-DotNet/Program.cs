﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticVariablleAndMethod
{
    //Creating Abstract class
    abstract class ArithmeticOperation
    {
        //Creating Abstract Method
        public abstract int Sum();
    }


    //Inheriting the Abstract Class
    class StaticMethod:ArithmeticOperation
    {
        //Creating Static Variables
        private static int num1 = 10;
        private static int num2 =20;
        
        //Overriding the Sum emthod from abstract class
        public override int Sum()
        {
            return num1 + num2;
        }

        //Creating Static Method
        public static int Mul()
        {
            return num1 * num2;
        }
    }

    //Inheriting SaticMethod class
    class Program
    {
        static void Main(string[] args)
        {
            StaticMethod method1 = new StaticMethod();
            //Calling the overriden method from abstract class
            Console.WriteLine(method1.Sum());
            //Calling the static method from Static Method Class
            Console.WriteLine(StaticMethod.Mul());
            Console.ReadLine();

        }
    }
}
